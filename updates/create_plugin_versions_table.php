<?php namespace JD\Autumn\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePluginVersionsTable extends Migration
{
    public function up()
    {
        Schema::create('jd_autumn_plugin_versions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('plugin_id')->unsigned();
            $table->string('version');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jd_autumn_plugin_versions');
    }
}
