<?php namespace Jd\Autumn\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectPluginsTable extends Migration
{
    public function up()
    {
        Schema::create('jd_autumn_project_plugins', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('project_id');
            $table->integer('plugin_id');
            $table->integer('status')->default(1);

            $table->primary(['project_id', 'plugin_id'], 'project_plugin_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('jd_autumn_project_plugins');
    }
}
