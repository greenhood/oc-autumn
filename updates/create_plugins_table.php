<?php namespace Jd\Autumn\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePluginsTable extends Migration
{
    public function up()
    {
        Schema::create('jd_autumn_plugins', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('author_name');
            $table->integer('type');
            $table->string('git_address')->nullable();
            $table->string('git_branch')->nullable();
            $table->string('http_address')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('jd_autumn_plugins');
    }
}
