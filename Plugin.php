<?php namespace JD\Autumn;

use Backend;
use System\Classes\PluginBase;
use RainLab\User\Models\User as UserModel;

/**
 * Autumn Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Autumn',
            'description' => 'No description provided yet...',
            'author'      => 'JD',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        UserModel::extend(function($model)
        {
            $model->hasMany['projects'] = ['Jd\Autumn\Models\Project'];
            $model->hasMany['plugins'] = ['Jd\Autumn\Models\Plugin'];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'JD\Autumn\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'jd.autumn.some_permission' => [
                'tab' => 'Autumn',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'autumn' => [
                'label'       => 'Autumn',
                'url'         => Backend::url('jd/autumn/projects'),
                'icon'        => 'icon-leaf',
                'permissions' => ['jd.autumn.*'],
                'order'       => 500,

                'sideMenu' => [
                    'projects' => [
                        'label' => 'Projects',
                        'icon' => 'icon-leaf',
                        'permissions' => ['jd.autumn.*'],
                        'url' => Backend::url('jd/autumn/projects'),
                    ],
                    'plugins' => [
                        'label' => 'Plugins',
                        'icon' => 'icon-leaf',
                        'permissions' => ['jd.autumn.*'],
                        'url' => Backend::url('jd/autumn/plugins'),
                    ],
                ]
            ],
        ];
    }

}
