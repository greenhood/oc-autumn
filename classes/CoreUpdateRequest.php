<?php namespace Jd\Autumn\Classes;

use Illuminate\Http\Request;

/**
 * Process request for CoreUpdate
 */
class CoreUpdateRequest
{
    private $request = null;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function getCore() {
        return $this->request->input("core");
    }

    public function getPlugins() {
        return unserialize($this->getPluginsOriginal());
    }

    public function getPluginsOriginal() {
        return $this->request->input("plugins");
    }

    public function getBuild() {
        return $this->request->input("build");
    }
    
    public function getForce() {
        return $this->request->input("force");
    }

    public function getServer() {
        return unserialize(base64_decode($this->getServerOriginal()));
    }

    public function getServerOriginal() {
        return $this->request->input("server");
    }

    public function __toString()
    {
        return json_encode($this->request->all());
    }
}