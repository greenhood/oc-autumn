<?php namespace Jd\Autumn\Classes;

use Proxy\Proxy;
use Proxy\Filter\RemoveEncodingFilter;
use Proxy\Adapter\Guzzle\GuzzleAdapter;
use Zend\Diactoros\ServerRequestFactory;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Abstract proxy request to OctoberCMS.com
 */
class ProxyToOctoberCMS
{
    const OCTOBER_CMS_URL = 'https://octobercms.com/';

    private $response = null;

    public function __construct() {
        $guzzle = new GuzzleClient();

        $this->proxyRequest = ServerRequestFactory::fromGlobals();
        
        $this->proxy = new Proxy(new GuzzleAdapter($guzzle));
        $this->proxy->filter(new RemoveEncodingFilter());
    }

    public function proxy() {
        $this->response = $this->proxy->forward($this->proxyRequest)->to(self::OCTOBER_CMS_URL);
        return $this;
    }

    public function response() {
        if(is_null($this->response)) {
            throw new Exception("Need to call proxy() before response()");
        }

        return response($this->response->getBody()->getContents(), $this->response->getStatusCode(), $this->response->getHeaders());
    }
}