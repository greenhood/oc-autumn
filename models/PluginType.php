<?php

namespace Jd\Autumn\Models;

use Illuminate\Support\Collection;

class PluginType
{
	const GIT = 1;
	const UPLOAD = 2;
	const HTTP = 3;

	public static function get()
	{
		return new Collection([
			self::GIT => 'Git',
			self::UPLOAD => 'Upload',
			self::HTTP => 'HTTP',
		]);
	}

	public static function find($id)
	{
		return self::get()->find($id);
	}
}