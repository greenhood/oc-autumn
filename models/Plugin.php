<?php 

namespace Jd\Autumn\Models;

use Model;
use Jd\Autumn\Models\PluginType;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Plugin Model
 */
class Plugin extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jd_autumn_plugins';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $appends = [
        'alias',
    ];

    public $rules = [
        'name' => 'required',
        'author_name' => 'required',
        'type' => 'required',
        'user_id' => 'required',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];

    public $hasMany = [
        'versions' => [\JD\Autumn\Models\PluginVersion::class],
    ];

    public $belongsTo = [
        'user' => \RainLab\User\Models\User::class,
    ];
    public $belongsToMany = [
        'projects' => [
            \JD\Autumn\Models\Project::class,
            'table' => 'jd_autumn_project_plugins',
            'pivot' => ['status'],
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getTypeOptions()
    {
        return PluginType::get()->toArray();
    }

    public function getAliasAttribute()
    {
        return $this->attributes['author_name'] . '.' . $this->attributes['name'];
    }

    public function setGitAddressAttribute($value)
    {
        if(! $value)
        {
            $this->attributes['git_address'] = null;
        }

        $this->attributes['git_address'] = $value;
    }

    public function setGitBranchAttribute($value)
    {
        $this->attributes['git_branch'] = $value;

        if(! $value && ! $this->attributes['git_address'])
        {
            $this->attributes['git_branch'] = null;    
        }

        if(! $value && $this->attributes['git_address'])
        {
            $this->attributes['git_branch'] = 'master';
        }
    }

    public function setHttpAddressAttribute($value)
    {
        $this->attributes['http_address'] = $value;

        if(! $value)
        {
            $this->attributes['http_address'] = null;
        }
    }

    public static function findByNameOrFail(
        $name,
        $columns = array('*')
    ) {
        if ( ! is_null($plugin = static::whereName($name)->first($columns))) {
            return $plugin;
        }

        throw new ModelNotFoundException;
    }

}